module.exports = {
  paths: {
    public: "../static/"
  },
  sourceMaps: false,
  files: {
    stylesheets: { joinTo: "app-1.4.css" }
  },
  plugins: {
    less: {},
    postcss: {
      processors: [
        // compatibility prefixes
        require("autoprefixer")(),
        // removes unused styles
        require("csswring")(),
        // minimize
        require("cssnano")({ preset: "default" })
      ]
    }
  }
};

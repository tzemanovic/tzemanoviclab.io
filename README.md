Tomáš Zemanovič's blog page
=============

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://spacemacs.org) [![Build Status](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/badges/master/build.svg)](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/pipelines)

Source code for [tzemanovic.gitlab.io](https://tzemanovic.gitlab.io).

This page is built with [Hakyll](https://jaspervdj.be/hakyll/) and [Brunch](https://brunch.io/) with [Pure.css](https://purecss.io/).

Brunch is used to build the stylesheets (from the [src/style-src](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/tree/master/src/style-src) directory), while [Hakyll](https://gitlab.com/tzemanovic/tzemanovic.gitlab.io/blob/master/src/Main.hs) runs Pandoc on the markdown posts sources to render them into HTML and then puts everything together.

## Build Stylesheet with Brunch

[Install brunch](https://brunch.io/docs/getting-started#tasting-your-first-brunch)

```
cd src/style-src
brunch build --production
```

## Build Blog Hakyll

[Install stack](https://docs.haskellstack.org/en/stable/install_and_upgrade/)

```
cd src
stack install --only-dependencies
stack build
stack exec site build
```

## Develop (run watch on Brunch and Hakyll sources)

```
cd src/style-src
brunch watch
```

and

```
cd src
stack exec site watch
```

## Publish

Push to GitLab, [CI](.gitlab-ci.yml) will do the rest.
